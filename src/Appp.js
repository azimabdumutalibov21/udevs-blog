
import { useEffect, useState } from 'react';
import './Appp.css';
import { db } from './Auth-JS/firebase'
import { collection, getDocs, addDoc, updateDoc, deleteDoc, doc } from 'firebase/firestore'
import { Link } from 'react-router-dom';

function Appp({id}) {
  const [newName] = useState('');
  const [newAge] = useState(0);

const [users, setUsers] = useState([]);
const usersCollectionRef = collection(db, 'users');

// const createUser = async () => {
// await addDoc(usersCollectionRef , { name:newName, age: Number(newAge) });
// }

// const updataUser = async (id, age) => {
// const userDoc = doc(db, 'users', id);
// const newFields = {age: age + 1};
// await updateDoc(userDoc, newFields);
// };

const deleteUser = async (id) => {
const userDoc = doc(db, 'users', id);
await deleteDoc(userDoc);
}

useEffect(() => {
  const getUsers = async () => {
const data = await getDocs(usersCollectionRef);
setUsers(data.docs.map((doc) =>({...doc.data(), id: doc.id})))
console.log(data);

  }
getUsers()

}, [])

  return (
    <div className="App">
      {/* <input placeholder='Name...' onChange={(event) => {setNewName(event.target.value)}}/>
      <input type='number' placeholder='Age...' onChange={(event) => {setNewAge(event.target.value)}}/>
      <button onClick={createUser}>Create User</button> */}
    {users.map((user, id) => {
      return (
   <div className='Post'>

   
      <div className='mainCont'>
          
          <div className='wrapp'>
<Link to={`post/${id}`}>
          <img className='img' width='250px' height='250px' src={user.image}/>
          <div className='gae'> {user.age}</div>
          <div className='name'> {user.name}</div>
</Link>
          {/* <button onClick={() => {
            updataUser(user.id, user.age);
          }}> Increase age</button>  */}
           { <button onClick={() => {deleteUser(user.id)}}>Delete User</button> }
          </div>
        </div>
      </div>
      )
    })}
    </div>
  );
}

export default Appp;
