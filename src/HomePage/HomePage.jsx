import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./style.css";
export default function HomePage() {
    const [isOpen, setIsOpen] = useState(false);
const isAuthorized = localStorage.getItem('isAuthorized' ,false)

function onSubmit(event) {
    event.preventDefault();
    console.log('submit');
    localStorage.setItem('isAuthorized', true)
}

	return (
		<div className="app">
			<h1>Home</h1>
			
			<div className="btn-block ">
                <Link to="/home">Go to blog</Link>
				{!isAuthorized && (<button onClick={() => setIsOpen(true)}>Login</button>)}
                
			</div>

			<div className={`box  ${isOpen ? 'visible' : ''}`}>
				<button onClick={() => setIsOpen(false)} className='close'>x</button>
				<h3>Login</h3>
				<form onSubmit={onSubmit}>
					<input type="text" name="email" />
					<input type="password" name="password" />
                    <button type='submit'>Send</button>
				</form>
			</div>
           {isOpen &&  <div className='overlay'/>}
		</div>
	);
}
