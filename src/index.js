import React from 'react';
import ReactDOM from 'react-dom';
import App from './Auth-JS/Epp'
// import App from './Hpp'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
