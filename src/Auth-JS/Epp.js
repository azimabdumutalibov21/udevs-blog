import { Container, Row, Col } from "react-bootstrap";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";
// import Home from "./components/Home";
import Login from "./components/Login";
import Signup from "./components/Signup";
import ProtectedRoute from "./components/ProtectedRoute";
import { UserAuthContextProvider } from "./context/UserAuthContext";
import Home from "../MainFigma/Figma/Figma";
import Blogs from "../Container/Blogs";
import Blogs1 from "../Container/Blogs1";
import Blogs2 from "../Container/Blogs2";
import Blogs3 from "../Container/Blogs3";
import Footer from "../Container/footer";
import NewPage from "../Container/newPage";
function Tpp() {
  return (
    <Router>
      {/* <Container style={{ width: "400px" }}> */}
      <Row>
        <Col>
          <UserAuthContextProvider>
            <Routes>
              <Route
                path="/home"
                element={
                  <ProtectedRoute>
                    <Home />
                  </ProtectedRoute>
                }
              />
              <Route path={`/home/post/${0}`} element={<Blogs />} />
              <Route path={`/home/post/${1}`} element={<Blogs1 />} />
              <Route path={`/home/post/${2}`} element={<Blogs2 />} />
              <Route path={`/home/post/${3}`} element={<Blogs3 />} />
              <Route path={`/profil`} element={<Footer />} />
              <Route path={`/newpage`} element={<NewPage />} />
              <Route path="/" element={<Login />} />
              <Route path="/signup" element={<Signup />} />
            </Routes>
          </UserAuthContextProvider>
        </Col>
      </Row>
      {/* </Container> */}
    </Router>
  );
}

export default Tpp;
