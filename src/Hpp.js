import React from 'react'
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Blogs from './Container/Blogs'
import Blogs1 from './Container/Blogs1'
import Blogs2 from './Container/Blogs2'
import Blogs3 from './Container/Blogs3'
import Figma from './MainFigma/Figma';
import  Auth  from './Auth-JS/Epp';

export default function Rpp({id}) {
    return (
        <Router>
             <div>
            <Routes>
                {/* <Route path='/' element={<HomePage/>}/> */}
                {/* <Route path='/' element={<Auth/>}/> */}
                <Route path='/' element={<Figma/>}/>
                <Route path={`/post/${id}`} element={<Blogs/>}/>
                <Route path={`/post/${0}`} element={<Blogs/>}/>
                <Route path={`/post/${1}`} element={<Blogs1/>}/>
                <Route path={`/post/${2}`} element={<Blogs2/>}/>
                <Route path={`/post/${3}`} element={<Blogs3/>}/>
            </Routes>
        </div>
        </Router>
       
    )
}
