import React from 'react'
import { MainLower,Box1,Box2, Text1,Text2,Text3,Ros } from './Lower'


export default function Lower() {
    return (
        <MainLower>
        <Box1>
            <a href="https://udevs.io/">
                <img className="str" src={udevs1}></img>
            </a>
            <Text1>
                Помощник в публикации статей, журналов.
                <br />
                Список популярных международных конференций.<br /> Всё для студентов и
                преподавателей.
            </Text1>
            <Text2>Copyright © 2020. LogoIpsum. All rights reserved.</Text2>
        </Box1>
        <Box2>
            <Ros>Ресурсы</Ros>

            <Text3>Статьи</Text3>
            <Text3>Журналы</Text3>
            <Text3>Газеты</Text3>
            <Text3>Газеты</Text3>
        </Box2>
        <Box2>
            <Ros>О нас</Ros>
            <Text3>Контакты</Text3>
            <Text3>Помощь</Text3>
            <Text3>Заявки</Text3>
            <Text3>Политика</Text3>
        </Box2>
        <Box2>
            <Ros>Помощь</Ros>

            <Text3>Часто задаваемые вопросы</Text3>
        </Box2>
    </MainLower>
    )
}
