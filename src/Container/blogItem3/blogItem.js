import React from "react";
import "./blogItem.css";
import userImg from "../images/userImg.png";
import cardImgL from "../images/semchka.jpg";
import cardImg from "../images/semchka.jpg";
import udevs from '../images/logo.png'
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import { BsFillBookmarkFill } from "react-icons/bs";
import  { MainLower,Box1,Box2, Text1,Text2,Text3,Ros } from '../Lower/Lower';


export default function BlogItem() {
  const [posts, setPosts] = useState([]);
  const { id } = useParams();
  useEffect(() => {
    getPosts();
  });

  function getPosts() {
    axios
      .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then(({ data }) => {
        setPosts(data);
        console.log(data);
      })
      .catch((error) => console.log(error));
  }

  return (
    <div>

    <div className="container blog-item__container">
      <div className="blog-item__user">
        <img className="user-image" src={userImg} alt="user" />
        <span className="user-name">Dilorom Aliyeva</span>
          <Link to={`/profil`}>
        <div className="user-buttons">
          <button className="btn btn-follow">Follow</button>
          <button className="btn btn-saved"><BsFillBookmarkFill/> </button>
        </div>
          </Link>
      </div>
      <div className="blog-item__content">
        <img src={cardImgL} alt="card" className="blog-item__img" />
        <span className="image-owner">
          Фото: <i>Dilorom Alieva</i>
        </span>
        <div className="card-info">
          <p className="card-date">18:26 11.01.2021</p>
          <span className="card-number">365</span>
        </div>
        <h2 className="blog-item__title">{posts.title}</h2>
        <p className="blog-item__text">{posts.body}</p>
      </div>
      <div className="blog-item__famous">
        <Link to={`/blogs`} className="card">
          <img
            className="card-img-top"
            src={cardImg}
            alt="card img"
            style={{ width: "203px", height: "130px"}}
            />
          <div className="card-body">
            <h6 className="card-title">Министерство инвестиций и внешней торговли, Министерство экономического </h6>
            <div className="card-info">
              <p className="card-date"></p>
              <span className="card-number"></span>
            </div>
          </div>
        </Link>
        <Link to={`/blogs`} className="card">
          <img
            className="card-img-top"
            src={cardImg}
            alt="card img"
            style={{ width: "203px", height: "130px"}}
            
            />
          <div className="card-body">
          <h6 className="card-title">Министерство инвестиций и внешней торговли, Министерство экономического </h6>

            <div className="card-info">
              <p className="card-date"></p>
              <span className="card-number"></span>
            </div>
          </div>
        </Link>
        <Link to={`/blogs`} className="card">
          <img
            className="card-img-top"
            src={cardImg}
            alt="card img"
            style={{ width: "203px", height: "130px"}}
            
          />
          <div className="card-body">
          <h6 className="card-title">Министерство инвестиций и внешней торговли, Министерство экономического </h6>

            <div className="card-info">
              <p className="card-date"></p>
              <span className="card-number"></span>
            </div>
          </div>
        </Link>
        <Link to={`/blogs`} className="card">
          <img
            className="card-img-top"
            src={cardImg}
            alt="card img"
            style={{ width: "203px", height: "130px"}}
            
            />
          <div className="card-body">
          <h6 className="card-title">Министерство инвестиций и внешней торговли, Министерство экономического </h6>

            <div className="card-info">
              <p className="card-date"></p>
              <span className="card-number"></span>
            </div>
          </div>
        </Link>
      </div>
    </div>
    <div className="mainlowe">

<MainLower><Box1>
        <a href="https://udevs.io/">
        <img className="str" src={udevs}></img>
        </a>
        <Text1>
        Помощник в публикации статей, журналов.
        <br />
        Список популярных международных конференций. Всё для студентов и
        преподавателей.
        </Text1>
        <Text2>Copyright © 2020. LogoIpsum. All rights reserved.</Text2>
        </Box1>
    <Box2>
        <Ros>Ресурсы</Ros>
        
        <Text3>Статьи</Text3>
        <Text3>Журналы</Text3>
        <Text3>Газеты</Text3>
        <Text3>Газеты</Text3>
        </Box2>
        <Box2>
        <Ros>О нас</Ros>
        <Text3>Контакты</Text3>
        <Text3>Помощь</Text3>
        <Text3>Заявки</Text3>
        <Text3>Политика</Text3>
        </Box2>
        <Box2>
        <Ros>Помощь</Ros>
        
        <Text3>Часто задаваемые вопросы</Text3>
      </Box2></MainLower>
</div>
            </div>
  );
}
