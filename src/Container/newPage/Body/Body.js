import './body.scss'
import { useEffect, useState } from 'react';
import { db } from '../../../Auth-JS/firebase'
import { collection, getDocs, addDoc, updateDoc, deleteDoc, doc } from 'firebase/firestore'
import { Link } from 'react-router-dom';

function Appp({id}) {
  const [newName, setNewName] = useState('');
  const [newAge, setNewAge] = useState(0);
  const [newImg, setNewImg] = useState('');

const [users, setUsers] = useState([]);
const usersCollectionRef = collection(db, 'users');

const createUser = async () => {
await addDoc(usersCollectionRef , { name:newName, age: Number(newAge), image:newImg });
}

const updataUser = async (id, age) => {
const userDoc = doc(db, 'users', id);
const newFields = {age: age + 1};
await updateDoc(userDoc, newFields);
};

const deleteUser = async (id) => {
const userDoc = doc(db, 'users', id);
await deleteDoc(userDoc);
}

useEffect(() => {
  const getUsers = async () => {
const data = await getDocs(usersCollectionRef);
setUsers(data.docs.map((doc) =>({...doc.data(), id: doc.id})))
console.log(data);

  }
getUsers()

}, [])

  return (

    <div className="App">
        <div className='create'>
        <h4 >Название</h4>
            <input placeholder='Text input...' onChange={(event) => {setNewName(event.target.value)}}/>
            <h4 >Описание</h4>
      <input type='number' placeholder='Number input...' onChange={(event) => {setNewAge(event.target.value)}}/>
      <h4 >Описание</h4>
      <input type='text' placeholder='Url input...' onChange={(event) => {setNewImg(event.target.value)}}/>
      <button type="submit"className="btn btn-outline btn-outline-primary btn-sm button"  onClick={createUser}><h5>Create User</h5> </button>
        </div>
      

    {users.map((user, id) => {
      return (
   <div className='Post'>

   
      <div className='mainCont'>
          
          <div className='wrapp'>
{/* <Link to={`post/${id}`}>
          <img className='img' width='250px' height='250px' src={user.image}/>
          <div className='gae'> {user.age}</div>
          <div className='name'> {user.name}</div>
</Link> */}
          {/* <button onClick={() => {
            updataUser(user.id, user.age);
          }}> Increase age</button>  */}
           {/* { <button onClick={() => {deleteUser(user.id)}}>Delete User</button> } */}
          </div>
        </div>
      </div>
      )
    })}
    </div>
  );
}

export default Appp;
