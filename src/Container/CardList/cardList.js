

import "./cardList.css";
import React from "react";
import Items from "../Items/Items";
import axios from "axios";
import { useState, useEffect } from "react";
import { db } from "../../firebase";
import {collection, getDocs} from '@firebase/firestore'
export default function CardList() {
  const [posts, setPosts] = useState([]);
  const postsCollectionRef = collection(db, 'posts')

  useEffect(() => {
    getPostsFromStore();
  }, []);

  function getPosts() {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then(({ data }) => {
        setPosts(data);
      })
      .catch((error) => console.error(error));
  }

  function getPostsFromStore() {
    getDocs(postsCollectionRef).then((res) => {
      setPosts(res.docs.map((doc) =>  ({...doc.data(), id: doc.id})))
      console.log(res.docs.map((doc) => ({...doc.data(), id: doc.id})));
    })
  }



  return (
    <div className="container">fdfdfdf
      <div className="d-grid blog-list">
        {posts.map((item) => (
          <Items key={item.id} title={item.title} id={item.id} />
        // <li key={item.id}>{item.id} {item.title}</li>
          
        )).splice(0,12)}
      </div>
    </div>
  );
}
