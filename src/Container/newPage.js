import React from 'react'
import Head from './newPage/Header/Head'
import Body from './newPage/Body/Body'
import style from './newPage.module.scss'
export default function newPage() {
    return (
        <div className={style.page}>
<Head/>
<Body/>
        </div>
    )
}
