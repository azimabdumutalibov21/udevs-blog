import React from 'react'
import BlogItem from './blogItem3/blogItem'
import HomeHeader from './homeHeader/homeHeader'
export default function Blogs() {
    return (
        <div>
            <HomeHeader/>
        <BlogItem/>
        </div>
    )
}
