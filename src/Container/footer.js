import React, { useState } from 'react'
import Body from './newPth/body/Body'
import Woman from './newPth/Woman/woman'
import Head from './newPth/Header/Header'
import Below from './newPth/Below/Below'
import style from './footer.module.scss'

export default function Footer() {
    const [selected, setSelected] = useState('');
    return (
        <div className={style.footer}>
            <Head selected={selected} setSelected={setSelected}/>
            <Woman/>
            <Body/>
            <Below/>
        </div>
    )
}
