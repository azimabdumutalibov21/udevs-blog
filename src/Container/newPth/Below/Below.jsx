import React from 'react'
import udevs from '../../images/logo.png'

import style from './below.module.scss'
export default function Below() {
    return (
        <div className={style.Below}>
            <div className={style.box}>
                <div className={style.image}>
                    <img src={udevs} alt="" />
                    <div className={style.text}>Помощник в публикации статей, журналов. 
Список популярных международных конференций.
Всё для студентов и преподавателей.</div>
            <div className={style.all}>Copyright © 2020. LogoIpsum. All rights reserved.</div>
                </div>
                <div className={style.box2}>
                <div className={style.txt}>Ресурсы</div>
                <div className={style.tex}>Статьи</div>
                <div className={style.tex}>Журналы</div>
                <div className={style.tex}>Статьи</div>
                <div className={style.tex}>Диплом</div>
                </div>
                <div className={style.box2}>
                <div className={style.txt}>О нас</div>
                <div className={style.tex}>Контакты</div>
                <div className={style.tex}>Помощь</div>
                <div className={style.tex}>Заявки</div>
                <div className={style.tex}>Политика</div>
                </div>
                <div className={style.box2}>
                <div className={style.txt}>Помощь</div>
                <div className={style.tex}>Часто задаваемые<br/> вопросы</div>
               
                </div>
            </div>
        </div>
    )
}
