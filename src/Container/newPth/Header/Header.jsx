import React, { useState } from 'react'
import './Header.scss'
import udevs from '../../images/logo.png'
import photo from '../../images/photo.png'
import { Link } from 'react-router-dom';
export default function Header({selected, setSelected}) {

  const [isActive, setIsActive] = useState(false);
  const options = ['Написать публикацию', 'Избранные', 'Выйти']
    return (
        <div className='navbar'>

<span className='image btn btn-outline btn-outline-primary btn-sm'><img src={udevs} alt="" /></span>
            <ul>

          <a className='btn btn-outline btn-outline-primary btn-sm' href="">Все потоки</a>
          <a className='btn btn-outline btn-outline-primary btn-sm' href="">Разработка</a>
          <a className='btn btn-outline btn-outline-primary btn-sm' href="">Администрирование</a>
          <a className='btn btn-outline btn-outline-primary btn-sm' href="">Дизайн</a>
          <a className='btn btn-outline btn-outline-primary btn-sm' href="">Менеджмент</a>
          <a className='btn btn-outline btn-outline-primary btn-sm' href="">Маркетинг</a>
          <a className='btn btn-outline btn-outline-primary btn-sm' href="">Научпоп</a>
            </ul>
          <button type="submit"className="btn btn-outline btn-outline-primary btn-sm"></button>

          <div className='dropdown'>
            <div className="dropdown-btn" onClick={(e) => setIsActive(!isActive)}><img src={photo} alt="" /></div>


            {isActive && (<div className="dropdown-content">

              {options.map((option) => (
         <Link to={'/newpage'}>
          <div onClick={(e) => setSelected(option) 
           } className="dropdown-item">
                {option}
              </div>
              </Link> 
              ))}
            </div>
            )}
          </div>
        </div>

    );
}
