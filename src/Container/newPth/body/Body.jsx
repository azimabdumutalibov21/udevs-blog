import React from 'react'
import { BsFillEyeFill } from "react-icons/bs";
import logo from '../../images/147.png'
import style from './body.module.scss'
export default function Page() {
    
    // const className = isActive ? 'btn-info' : 'btn-outline-secondary';
    return (

        <>
        <div className={style.body}>
            <h1>ПУБЛИКАЦИИ</h1>
            <div className={style.image}>
                <img src={logo}/>
                <div className={style.title}>
                    <h2 className={style.h2}>Министров № 294 утверждены Положение о Молодежной индустриально-предпринимательской зоне и Фонде поддержки молодых</h2>
                   
                    <div className={style.number}><p>18.34 11.04.2010 </p> <p> <BsFillEyeFill/> 369</p>
                    <p></p>Права человека </div>
                    <h5>Посланник Генерального секретаря ООН по делам молодежи Джаятма Викраманаяке приняла участие в презентации созданной по инициативе Узбекистана Группе друзей по правам молодежи. В рамках этого международного проекта планируется продвижение прав молодых жителей планеты и расшире...</h5>
                    {/* <button ></button> */}
                {/* <button className={`btn ${className}`}></button>     */}
                </div>
            </div>
          
        </div>

        <div className={style.body}>
            <h1>ПУБЛИКАЦИИ</h1>
            <div className={style.image}>
                <img src={logo}/>
                <div className={style.title}>
                    <h2 className={style.h2}> Утвердить предложение о создании молодежных индустриальных зон). В частности, в Нукусе и Берунийском</h2>
                   
                    <div className={style.number}><p>18.34 11.04.2010 </p> <p> <BsFillEyeFill/> 369</p>
                    <p></p>Права человека </div>
                    <h5>Посланник Генерального секретаря ООН по делам молодежи Джаятма Викраманаяке приняла участие в презентации созданной по инициативе Узбекистана Группе друзей по правам молодежи. В рамках этого международного проекта планируется продвижение прав молодых жителей планеты и расшире...</h5>
                    {/* <button ></button> */}
                    <button type="submit" className="btn btn-outline btn-outline-primary btn-sm button"><h2>Читать</h2> </button>
                </div>
            </div>
        </div>

        <div className={style.body}>
            <h1>ПУБЛИКАЦИИ</h1>
            <div className={style.image}>
                <img src={logo}/>
                <div className={style.title}>
                    <h2 className={style.h2}>Фонде поддержки молодых предпринимателей. Молодые предприниматели, желающие разместить свои </h2>
                   
                    <div className={style.number}><p>18.34 11.04.2010 </p> <p> <BsFillEyeFill/> 369</p>
                    <p></p>Права человека </div>
                    <h5>Посланник Генерального секретаря ООН по делам молодежи Джаятма Викраманаяке приняла участие в презентации созданной по инициативе Узбекистана Группе друзей по правам молодежи. В рамках этого международного проекта планируется продвижение прав молодых жителей планеты и расшире...</h5>
                    {/* <button ></button> */}
                    <button type="submit" className="btn btn-outline btn-outline-primary btn-sm button"><h2>Читать</h2> </button>
                </div>
            </div>
        </div>

        <div className={style.body}>
            <h1>ПУБЛИКАЦИИ</h1>
            <div className={style.image}>
                <img src={logo}/>
                <div className={style.title}>
                    <h2 className={style.h2}>“Сарбиноз” в Нукусе. В беседе с членами рабочей группы жительница махалли Алия Танжарбоева сообщила</h2>
                   
                    <div className={style.number}><p>18.34 11.04.2010 </p> <p> <BsFillEyeFill/> 369</p>
                    <p></p>Права человека </div>
                    <h5>Посланник Генерального секретаря ООН по делам молодежи Джаятма Викраманаяке приняла участие в презентации созданной по инициативе Узбекистана Группе друзей по правам молодежи. В рамках этого международного проекта планируется продвижение прав молодых жителей планеты и расшире...</h5>
                    {/* <button ></button> */}
                    <button type="submit" className="btn btn-info btn-info btn-outline-secondary"><h2>Читать</h2> </button>
                </div>
            </div>
        </div>
        </>
    )
}
